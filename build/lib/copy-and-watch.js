var gulp = require('gulp');
var gulpWatch = require('gulp-watch');
var commander = require('./commander');

module.exports = function (src, dest) {
    return function () {
        if (commander.watch) {
            return gulp.src(src)
                .pipe(gulpWatch(src))
                .pipe(gulp.dest(dest));
        }

        return gulp.src(src).pipe(gulp.dest(dest));
    };
};

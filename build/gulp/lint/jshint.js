'use strict';

var gulp = require('gulp');
var gulpJshint = require('gulp-jshint');

module.exports = function () {
    return gulp.src([
        'gulpfile.js',
        'src/js/**.js'
    ])
        .pipe(gulpJshint())
        .pipe(gulpJshint.reporter('default'));
};

'use strict';

var mac = require('mac');
var docsBuild = require('./docs/build');
var docsServer = require('./docs/server');

module.exports = mac.series(
    docsBuild,
    docsServer
);

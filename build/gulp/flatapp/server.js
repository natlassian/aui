'use strict';

var commander = require('commander');
var gulp = require('gulp');
var gulpWebserver = require('gulp-webserver');

commander
    .option('-h, --host [0.0.0.0]', 'The host to listen on.')
    .option('-p, --port [7000]', 'The port to listen on.');

module.exports = function () {
    return gulp.src('.tmp/flatapp/target/static')
        .pipe(gulpWebserver({
            host: commander.host || '0.0.0.0',
            livereload: true,
            open: 'pages',
            port: commander.port || 7000
        }));
};

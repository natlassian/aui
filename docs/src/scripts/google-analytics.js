/*
 This snippet is from google, don't modify this, ga global should still exist so if we want to use it from script
 tags inside html we can
 */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

var GA_TRACKING_CODE = 'UA-6032469-23';

var ga = window.ga;

ga('create', GA_TRACKING_CODE, 'auto');

export default ga;


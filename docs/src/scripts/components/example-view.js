'use strict';

import './code';
import Sandy from 'sandy';
import skate from 'skatejs';

/**
 * A web component that renders full working examples using <aui-docs-code>, with the ability to post the code
 * blocks to jsbin, jsfiddle, and codepen.
 * Also includes a button that enables the example to be viewed in various sandboxing websites (jsbin, jsfiddle etc.)
 *
 * Example:
 * <aui-docs-example>
 *      <script is="aui-docs-code" type="text/html">
 *          some HTML
 *      </script>
 * </aui-docs-example>
 *
 * The contents of the <aui-docs-example> should be a collection <script is="aui-docs-code"> elements.
 *
 * # Attributes
 * - live-demo: (boolean attribute) if set, render the demo in the docs with a live executing example.
 *
 * # Hiding code blocks from demos
 * If you wish to hide a code block for docs display, but need the code for the demo (live or jsbin), you can remove the
 * `is="aui-docs-code"` attribute from the script tag, which will prevent rendering of the code listing on the docs pages.
 */

skate('aui-docs-example', {
    created: function(el) {
        el.className = 'aui-docs-example';

        generateLiveDemo(el);

        var exampleActions = createExampleActions(el);
        el.appendChild(exampleActions);
    }
});

function generateLiveDemo (el) {
    if (!el.hasAttribute('live-demo')) {
        return;
    }

    document.addEventListener('DOMContentLoaded', function () {
        var languages = getLanguageContents(el);

        if (languages.html) {
            var liveDemo = document.createElement('aui-code-block');
            liveDemo.className = 'aui-live-demo';
            liveDemo.innerHTML = languages.html;
            el.insertBefore(liveDemo, el.childNodes[0]);
        }

        if (languages.css) {
            var cssScript = document.createElement('style');
            cssScript.innerHTML = languages.css;
            document.head.appendChild(cssScript);
        }

        if (languages.js) {
            var jsScript = document.createElement('script');
            jsScript.innerHTML = languages.js;
            document.head.appendChild(jsScript);
        }
    });
}

function createExampleActions (el) {
    var id = AJS.id('aui-docs-example-actions-dropdown');
    var exampleActionsContent = `
        <div class="aui-buttons">
            <button class="aui-button aui-button-split-main aui-button-light" data-docs-example-destination="jsbin"><span class="aui-icon aui-icon-small aui-iconfont-edit">Edit icon</span> Edit in jsbin</button>
            <button class="aui-button aui-dropdown2-trigger aui-button-split-more aui-button-light" aria-haspopup="true" aria-controls="${id}" aria-expanded="false">Split more</button>
        </div>
        <div id="${id}" class="aui-dropdown2 aui-style-default aui-layer" aria-hidden="true">
            <ul class="aui-list-truncate">
                <li><a data-docs-example-destination="codepen" href="#">Edit in codepen</a></li>
                <li><a data-docs-example-destination="jsfiddle" href="#">Edit in jsfiddle</a></li>
            </ul>
        </div>
    `;

    var exampleActions = document.createElement('div');
    exampleActions.className = 'aui-docs-example-actions';
    exampleActions.innerHTML = exampleActionsContent;


    function exampleButtonClickHandler (e) {
        var languages = getLanguageContents(el);
        var sandyExample = createSandyExample(languages.html, languages.js, languages.css);
        sandyExample.pushTo(e.target.getAttribute('data-docs-example-destination'));
    }

    var buttons = exampleActions.querySelectorAll('[data-docs-example-destination]');

    for (var i = 0; i < buttons.length; i++) {
        buttons[i].addEventListener('click', exampleButtonClickHandler);
    }

    return exampleActions;
}

function getCodeElements (el, lang) {
    return el.querySelectorAll('[type="text/' + lang + '"]');
}

function getLanguageContents (el) {
    var languages = {};
    var cssScripts = getCodeElements(el, 'css');
    var htmlScripts = getCodeElements(el, 'html');
    var jsScripts = getCodeElements(el, 'js');

    languages.css = getInnerContent(cssScripts);
    languages.html = getInnerContent(htmlScripts);
    languages.js = getInnerContent(jsScripts);

    return languages;
}

function getInnerContent (elements) {
    if (elements.length>0) {
        var content = '';
        Array.prototype.forEach.call(elements, function (element) {
            content += element.innerHTML + '\n';
        });
        return content;
    }
    return null;
}

function createSandyExample(html, js, css) {
    var config = {
        dependencies: getDependencies()
    };

    if (html) {
        config.html = {content: html};
    }

    if (css) {
        config.css = {content: css};
    }

    if (js) {
        config.js = {content: js};
    }

    return new Sandy(config);
}

function getDependencies () {
    var distLocation = getDistLocation();
    var dependencies = [
        'http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js',
        'http://cdnjs.cloudflare.com/ajax/libs/require.js/2.1.15/require.min.js',
        'http://cdnjs.cloudflare.com/ajax/libs/sinon.js/1.15.4/sinon.min.js',
        distLocation + '/js/aui.js',
        distLocation + '/js/aui-experimental.js',
        distLocation + '/js/aui-datepicker.js',
        distLocation + '/css/aui.css',
        distLocation + '/css/aui-experimental.css'
    ];

    return dependencies;
}

function getDistLocation () {
    //Inserted by the template at run time based on the presence / absence of the --debug flag
    return document.getElementById('dist-location').innerHTML;
}

---
component: Inline dialog
layout: main-layout.html
---

<a href="https://design.atlassian.com/latest/product/components/inline-dialog/" class="aui-button aui-button-link docs-meta-link">Design guidelines</a>
<div class="aui-message aui-message-info">
    This documentation is for the <strong>web component inline dialog API</strong>.
    <ul class="aui-nav-actions-list"><li><a href="inline-dialog-deprecated.html">View documentation for the deprecated javascript-based inline dialog API.</a></li></ul>
</div>



<h3>Summary</h3>
<p>The inline dialog is a wrapper for secondary content/controls to be displayed on user request. Consider this component as displayed in context to the triggering control with the dialog overlaying the page content.</p>
<p>A inline dialog should be preferred over a modal dialog when a connection between the action has a clear benefit versus having a lower user focus.</p>



<h3>Status</h3>
<table class="aui summary">
    <tbody>
    <tr>
        <th>API status:</th>
        <td><span class="aui-lozenge aui-lozenge-success">general</span></td>
    </tr>
    <tr>
        <th>Included in AUI core?</th>
        <td><span class="aui-lozenge aui-lozenge-current">Not in core</span> You must explicitly require the web
            resource key.
        </td>
    </tr>
    <tr>
        <th>Web resource key:</th>
        <td class="resource-key" data-resource-key="com.atlassian.auiplugin:aui-inline-dialog2"><code>com.atlassian.auiplugin:aui-inline-dialog2</code>
        </td>
    </tr>
    <tr>
        <th>AMD Module key:</th>
        <td class="resource-key">N/A</td>
    </tr>
    <tr>
        <th>Experimental since:</th>
        <td>5.7</td>
    </tr>
    <tr>
        <th>General API status:</th>
        <td>5.9</td>
    </tr>
    </tbody>
</table>



<h3>Examples</h3>
<div class="aui-flatpack-example">
    <a data-aui-trigger href="#" aria-controls="inline-dialog-wc-1">Inline dialog trigger</a>
    <aui-inline-dialog id="inline-dialog-wc-1" alignment="bottom center">
        Inline dialog content example
    </aui-inline-dialog>
</div>



<h3>Code</h3>
<p>The HTML component is simply a trigger with an ID, which can be used by Inline Dialog's JavaScript.</p>
<aui-docs-example>
    <script is="aui-docs-code" type="text/html">
        <a data-aui-trigger href="#" aria-controls="inline-dialog-wc-1">Inline dialog trigger</a>
        <aui-inline-dialog id="inline-dialog-wc-1" alignment="bottom center">
            Inline dialog content example.
        </aui-inline-dialog>
    </script><!--</aui-docs-code>-->
</aui-docs-example>



<h3>Attributes and properties</h3>
<table class="aui" id="dialog-methods">
    <thead>
        <tr>
            <th>Name</th>
            <th>Attribute</th>
            <th>Property</th>
            <th>Type</th>
            <th class="description">Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>id</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
            <td>String</td>
            <td>
                <p>Required when using a Trigger to interact with the Inline Dialog but not required for the Inline Dialog to function.</p>
                <p>Defaults to <code>null</code>.</p>
            </td>
        </tr>
        <tr>
            <td><code>alignment</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">is not a property</span></td>
            <td>String</td>
            <td>
                <p>Specifies the inline dialog's alignment with respect to its trigger. The inline dialog is not positioned if this is not specified.</p>
                <p>Defaults to <code>"top right"</code>.</p>
                <p>
                    <div id="alignment-values-table" class="aui-expander-content" aria-hidden="true">
                        <table class="aui">
                            <tr class="top-row">
                                <td></td>
                                <td><span class="aui-lozenge">default</span> <code>top left</code></td>
                                <td><code>top center</code></td>
                                <td><code>top right</code></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td><code>left top</code></td>
                                <td colspan="3" rowspan="3" class="trigger-cell">
                                    Inline dialog trigger
                                </td>
                                <td><code>right top</code></td>
                            </tr>
                            <tr>
                                <td><code>left center</code></td>
                                <td><code>right center</code></td>
                            </tr>
                            <tr>
                                <td><code>left bottom</code></td>
                                <td><code>right bottom</code></td>
                            </tr>
                            <tr class="bottom-row">
                                <td></td>
                                <td><code>bottom left</code></td>
                                <td><code>bottom center</code></td>
                                <td><code>bottom right</code></td>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <a data-replace-text="Hide values" class="aui-expander-trigger" aria-controls="alignment-values-table">Show all values</a>
                </p>
            </td>
        </tr>
        <tr>
            <td><code>hidden</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is a property</span></td>
            <td>Boolean</td>
            <td>
                <p>When set it either hides or shows the element based on whether the incoming value is falsy or truthy. When accessed it will return whether or not the element is hidden.</p>
                <p>Defaults to <code>true</code>.</p>
            </td>
        </tr>
        <tr>
            <td><code>persistent</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">is not a property</span></td>
            <td>Boolean</td>
            <td>
                <p>Specifies that the inline dialog is persistent. Persistent inline dialogs cannot be closed by outside click or escape.</p>
                <p>Defaults to <code>false</code>.</p>
            </td>
        </tr>
        <tr>
            <td><code style="white-space: nowrap">responds-to</code></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-success">is an attribute</span></td>
            <td><span class="aui-icon aui-icon-small aui-iconfont-close-dialog">is not a property</span></td>
            <td>String</td>
            <td>
                <p>Determines the type of interaction a trigger will have with the inline dialog.</p>
                <p>
                    Values:
                    <ul>
                        <li><code>toggle</code> <span class="aui-lozenge">default</span> - will respond to click event on the trigger.</li>
                        <li><code>hover</code> - will respond to mouseover, mouseout, focus, blur events on the trigger.</li>
                    </ul>
                </p>
            </td>
        </tr>
    </tbody>
</table>



<h3>Methods</h3>
<p>There are no methods.</p>



<h3>Events</h3>
<p>Events are triggered when inline dialogs are shown and hidden. These events are triggered natively on the component. You can bind to the the inline dialog2 component for instance specific events, or rely on event bubbling and bind to the document to receive events for every show and hide.</p>
<table class="aui" id="inline-dialog-events">
    <thead>
        <tr>
            <th>Event</th>
            <th class="description">Description</th>
            <th>Preventable</th>
            <th>Bubbles</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>aui-show</td>
            <td>Triggered before an inline dialog instance is shown.</td>
            <td><strong>Yes</strong>. Prevents it from showing.</td>
            <td>Yes</td>
        </tr>
        <tr>
            <td>aui-hide</td>
            <td>Triggered before an inline dialog instance is hidden.</td>
            <td><strong>Yes</strong>. Prevents it from hiding.</td>
            <td>Yes</td>
        </tr>
    </tbody>
</table>

'use strict';

import globalize from './internal/globalize';

/**
 * Similar to Javascript's in-built escape() function, but where the built-in escape()
 * might encode unicode charaters as %uHHHH, this function will leave them as-is.
 *
 * NOTE: this function does not do html-escaping, see escapeHtml().
 */
function escape (string) {
    return escape(string).replace(/%u\w{4}/gi, function (w) {
        return unescape(w);
    });
}

globalize('escape', escape);

export default escape;

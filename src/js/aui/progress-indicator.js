'use strict';

import $ from './jquery';
import { recomputeStyle } from './internal/animation';
import globalize from './internal/globalize';

function updateProgress($progressBar, $progressBarContainer, progressValue) {
    recomputeStyle($progressBar);
    $progressBar.css("width", progressValue * 100 + "%");
    $progressBarContainer.attr("data-value", progressValue);
}

var progressBars = {
    update: function(element, value){
        var $progressBarContainer = $(element).first();
        var $progressBar = $progressBarContainer.children(".aui-progress-indicator-value");
        var currentProgress = $progressBar.attr("data-value") || 0;

        var afterTransitionEvent = "aui-progress-indicator-after-update";
        var beforeTransitionEvent = "aui-progress-indicator-before-update";
        var transitionEnd = "transitionend webkitTransitionEnd";

        var isIndeterminate = !$progressBarContainer.attr("data-value");

        //if the progress bar is indeterminate switch it.
        if (isIndeterminate) {
            $progressBar.css("width", 0);
        }

        if (typeof value === "number" && value<= 1 && value >= 0) {
            $progressBarContainer.trigger(beforeTransitionEvent, [currentProgress, value]);

            //detect whether transitions are supported
            var documentBody = document.body || document.documentElement;
            var style = documentBody.style;
            var transition = 'transition';

            //trigger the event after transition end if supported, otherwise just trigger it
            if (typeof style.transition === 'string' || typeof style.WebkitTransition === "string") {
                $progressBar.one(transitionEnd, function() {
                    $progressBarContainer.trigger(afterTransitionEvent, [currentProgress, value]);
                });
                updateProgress($progressBar, $progressBarContainer, value);
            } else {
                updateProgress($progressBar, $progressBarContainer, value);
                $progressBarContainer.trigger(afterTransitionEvent, [currentProgress, value]);
            }


        }
        return $progressBarContainer;
    },

    setIndeterminate: function(element) {
        var $progressBarContainer = $(element).first();
        var $progressBar = $progressBarContainer.children(".aui-progress-indicator-value");

        $progressBarContainer.removeAttr("data-value");
        recomputeStyle($progressBarContainer);
        $progressBar.css("width", "100%");
    }
};

globalize('progressBars', progressBars);

export default progressBars;

'use strict';

import * as log from '../../../../src/js/aui/internal/log';

describe('aui/internal/log', function () {
    it('globals', function () {
        expect(AJS.log).to.be.defined;
    });
});

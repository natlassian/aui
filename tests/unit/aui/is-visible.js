'use strict';

import isVisible from '../../../src/js/aui/is-visible';

describe('aui/is-visible', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            isVisible: isVisible
        });
    });
});

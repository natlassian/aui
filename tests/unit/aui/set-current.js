'use strict';

import setCurrent from '../../../src/js/aui/set-current';

describe('aui/set-current', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            setCurrent: setCurrent
        });
    });
});

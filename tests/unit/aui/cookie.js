'use strict';

import * as cookie from '../../../src/js/aui/cookie';

describe('aui/cookie', function () {
    it('globals', function () {
        expect(AJS.cookie).to.be.defined;
        expect(AJS.Cookie).to.be.defined;
    });
});

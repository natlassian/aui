'use strict';

import escape from '../../../src/js/aui/escape';

describe('aui/escape', function () {
    it('globals', function () {
        expect(AJS).to.contain({
            escape: escape
        });
    });
});
